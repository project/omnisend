# Omnisend

Omnisend provides the ecommerce email marketing, automated emails and SMS.
This integration provides the webform handler that will allow you to capture
and submit form data from Webform submissions to Omnisend for further email
marketing automation.

## Usage

Configure the Omnisend public and API key at `/admin/config/services/omnisend`

The dashboard pages are located at these urls respectively

 * Campaign dashboard page: `/admin/omnisend/campaigns`

To use the webformhandler, you need to specify the fields from which data
should be forwarded to Omnisend, the default fields can be chosen via
select list.

When using the custom yaml mapping the following syntax is required:
 * `field[omnisend_field_id,0]: '[webform_field_machine_name]'`.

Examples using Omnisend custom mapping:
 * Field ID: `field[345,0]: '[webform_field_machine_name]'`
 * Personalization Tag: `field[%PERS_1%,0]: '[webform_field_machine_name]'`

## Features:

 * Webform Submission Mapping: Easily map webform fields to Omnisend
 contact properties, ensuring that the right data is captured
 and synchronized.

 * Real-time Sync: The module enables real-time synchronization of
 webform submissions with Omnisend, ensuring that your contact database
 is always up to date.

 * Custom Field Mapping: In addition to standard fields, you can also map
 custom fields from your webform to Omnisend, allowing you to capture and
 utilize specific user information for personalized marketing campaigns.

 * Opt-in Management: Seamlessly manage opt-in preferences by allowing
 users to subscribe or unsubscribe from your email list directly through
 the webform.

 * Error Logging: The module includes comprehensive error logging to help
 you identify and troubleshoot any synchronization issues, ensuring
 data integrity.

 * Automation Triggers: Leverage Omnisend's automation features by
 configuring triggers based on webform submissions. Automatically send
 welcome emails, abandoned cart reminders, or any other automated campaigns
 based on user actions.

## Requirements:

 * Drupal 9.4 or above
 * [Webform module](https://www.drupal.org/project/webform)

More info can be found at the Omnisend API pages, [contact sync documentation](https://api-docs.omnisend.com/reference/get_campaigns-campaignid-contacts)
