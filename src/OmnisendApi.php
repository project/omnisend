<?php

namespace Drupal\omnisend;

use Drupal\Core\Config\ConfigFactoryInterface;

use GuzzleHttp\Client;

/**
 * Api service.
 */
class OmnisendApi {
  /**
   * The omnisend marketing api SDK.
   *
   * @var \Omnisend
   */
  protected $omnisendMarketingSDK;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The api key.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * Constructs an Api object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $config = $this->configFactory->get('omnisend.settings');

    $this->apiKey = $config->get('api_key') ?? '';
  }

  /**
   * Get lists via the omnisend marketing API.
   *
   * @param array $filter_options
   *   The filter options for the api call.
   * @param int $page
   *   The page to start from.
   * @param int $limit
   *   The number of records per page.
   */
  public function getLists(array $filter_options = [], int $page = 0, int $limit = 20) {
    $url = 'https://a.omnisend.com/api/lists/';
    $headers = [
      'Authorization' => 'Omnisend-API-Key ' . $this->apiKey,
      'accept' => 'application/json',
      'revision' => '2023-02-22',
    ];

    $response_data = [];

    try {

      $client = new Client();
      $response = $client->request('GET', $url, [
        'headers' => $headers,
      ]);

      $responseBody = $response->getBody()->getContents();

      $decoded_response = json_decode($responseBody);

      if (isset($decoded_response->data)) {
        $response_data = $decoded_response->data;
      }

    }
    catch (\Exception $e) {
      omnisend_log_messages("warning", $e->getMessage());
    }

    return $response_data;
  }

  /**
   * Get Campaigns via the omnisend marketing API.
   *
   * @param array $filter_options
   *   The filter options for the api call.
   * @param int $page
   *   The page to start from.
   * @param int $limit
   *   The number of records per page.
   */
  public function getCampaigns(array $filter_options = [], int $page = 0, int $limit = 20) {

    $url = 'https://api.omnisend.com/v3/campaigns?offset=0&limit=100';
    $headers = [
      'X-API-KEY' => $this->apiKey,
      'accept' => 'application/json',
    ];

    $response_data = [];

    try {

      $client = new Client();
      $response = $client->request('GET', $url, [
        'headers' => $headers,
      ]);

      $responseBody = $response->getBody()->getContents();

      $decoded_response = json_decode($responseBody);

      if (isset($decoded_response->campaign)) {
        $response_data = $decoded_response->campaign;
      }

    }
    catch (\Exception $e) {

    }

    return $response_data;

  }

  /**
   * Sends contact data to omnisend marketing using the omnisend marketing API.
   *
   * @param array $contact
   *   The data to send to the omnisend marketing site.
   */
  public function syncContact(array $contact) {
    $email = $contact['email'] ?? '';
    if ($email != '' && $this->apiKey != '') {
      $url = 'https://api.omnisend.com/v3/contacts';
      $headers = [
        'X-API-KEY' => $this->apiKey,
        'accept'       => 'application/json',
        'content-type' => 'application/json',
      ];

      $data = [
        'identifiers' => [
                [
                  'type' => 'email',
                  'channels' => [
                    'email' => [
                      'status' => 'subscribed',
                    ],
                  ],
                  'id' => $email,
                ],
        ],
      ];

      if (isset($contact['firstName']) && $contact['firstName'] != '') {
        $data['firstName'] = $contact['firstName'];

      }
      if (isset($contact['lastName']) && $contact['lastName'] != '') {
        $data['lastName'] = $contact['lastName'];

      }
      if (isset($contact['country']) && $contact['country'] != '') {
        $data['country'] = $contact['country'];

      }
      if (isset($contact['countryCode']) && $contact['countryCode'] != '') {
        $data['countryCode'] = $contact['countryCode'];

      }
      if (isset($contact['state']) && $contact['state'] != '') {
        $data['state'] = $contact['state'];

      }
      if (isset($contact['city']) && $contact['city'] != '') {
        $data['city'] = $contact['city'];

      }
      if (isset($contact['address']) && $contact['address'] != '') {
        $data['address'] = $contact['address'];

      }
      if (isset($contact['postalCode']) && $contact['postalCode'] != '') {
        $data['postalCode'] = $contact['postalCode'];

      }
      if (isset($contact['gender']) && $contact['gender'] != '') {
        $data['gender'] = $contact['gender'];

      }
      if (isset($contact['birthdate']) && $contact['birthdate'] != '') {
        $data['birthdate'] = $contact['birthdate'];

      }
      if (isset($contact['sendWelcomeEmail']) && $contact['sendWelcomeEmail'] != '') {
        $data['sendWelcomeEmail'] = $contact['sendWelcomeEmail'];

      }

      try {
        $client = new Client();
        $client->request('POST', $url, [
          'headers' => $headers,
          'json' => $data,
        ]);

        omnisend_log_messages("status", $this->t('Created new contact with email @email.', ['@email', $email]));
      }
      catch (\Exception $e) {
        omnisend_log_messages("warning", $e->getMessage());
      }
    }
  }

}
