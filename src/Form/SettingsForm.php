<?php

namespace Drupal\omnisend\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Omnisend settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('Api connection'),
    ];

    $form['api']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $this->config('omnisend.settings')->get('api_key'),
      '#description' => $this->t('Your API Key. See this <a href="https://support.omnisend.com/en/articles/1061890-generating-api-key" target="_blank">article</a> for more details.'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['omnisend.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'omnisend_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('omnisend.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
