<?php

namespace Drupal\omnisend\Form;

use Drupal\omnisend\OmnisendApi;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Pager\PagerManager;
use Drupal\Core\Pager\PagerParameters;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds a dashboard.
 */
class OmnisendDashboard extends FormBase {

  /**
   * The omnisend marketing api service.
   *
   * @var \Drupal\omnisend\OmnisendApi
   */
  protected $api;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManager
   */
  protected $pagerManager;

  /**
   * The pager parameters.
   *
   * @var \Drupal\Core\Pager\PagerParameters
   */
  protected $pagerParameters;

  /**
   * The omnisend marketing dashboard constructor.
   *
   * @param \Drupal\omnisend\OmnisendApi $omnisend_api
   *   The omnisend marketing api service.
   * @param \Drupal\Core\Pager\PagerManager $pagerManager
   *   The pager manager service.
   * @param \Drupal\Core\Pager\PagerParameters $pagerParameters
   *   The pager parameters service.
   */
  public function __construct(
    OmnisendApi $omnisend_api,
    PagerManager $pagerManager,
    PagerParameters $pagerParameters,
  ) {
    $this->api = $omnisend_api;
    $this->pagerManager = $pagerManager;
    $this->pagerParameters = $pagerParameters;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): OmnisendDashboard {
    return new static(
      $container->get('omnisend.api'),
      $container->get('pager.manager'),
      $container->get('pager.parameters'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $limit = 20;
    $page = $this->pagerParameters->findPage();
    $data = $this->getTableData($page, $limit);

    $form['table'] = [
      '#type' => 'table',
      '#header' => $data['table_fields'],
      '#empty' => $this->t('No items found'),
    ];
    // If there is data to display add the rows and pager.
    if (isset($data['rows'])) {
      $form['table']['#rows'] = $data['rows'];
      $this->pagerManager->createPager($data['total'], $limit);
      $form['pager'] = [
        '#type' => 'pager',
      ];
    }

    return $form;
  }

  /**
   * Get the data from the api to be shown in the table.
   *
   * @param int $page
   *   The page to start from.
   * @param int $limit
   *   The number of rows to display on every page.
   *
   * @return array
   *   The form render array with data from the omnisend marketing api.
   */
  protected function getTableData(int $page = 0, int $limit = 20): array {
    $data['total'] = 0;
    $data['table_fields'] = [];
    $data['rows'] = [];
    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'omnisend';
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
