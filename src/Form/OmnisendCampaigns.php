<?php

namespace Drupal\omnisend\Form;

/**
 * Adds a campaigns dashboard.
 */
class OmnisendCampaigns extends OmnisendDashboard {

  /**
   * {@inheritDoc}
   */
  protected function getTableData(int $page = 0, int $limit = 20): array {
    $data['table_fields'] = [
      $this->t('ID'),
      $this->t('Name'),
      $this->t('Type'),
    ];

    // Check if we have an object containing our data instead of an error
    // message.
    $response = $this->api->getCampaigns([], $page, $limit);
    if (is_string($response)) {
      $this->messenger()->addError($response);
    }
    else {
      $data['total'] = count($response);
      foreach ($response as $key => $campaign) {
        // Check if the row is actual data or metadata from the api.
        $data['rows'][$key] = [
          $campaign->campaignID,
          $campaign->name,
          $campaign->type,
        ];

      }
    }

    return $data;
  }

}
