<?php

namespace Drupal\omnisend\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Utility\WebformYaml;
use Drupal\webform\webformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "omnisend_contact",
 *   label = @Translation("Omnisend"),
 *   category = @Translation("Automated marketing"),
 *   description = @Translation("Send submission data to Omnisend"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class OmnisendFormHandler extends WebformHandlerBase {

  /**
   * The omnisend marketing api service.
   *
   * @var \Drupal\omnisend\OmnisendApi
   */
  protected $omnisendMarketingApi;

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->omnisendMarketingApi = $container->get('omnisend.api');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'email_field' => NULL,
      'first_name_field' => NULL,
      'last_name_field' => NULL,
      'country_field' => NULL,
      'country_code_field' => NULL,
      'state_field' => NULL,
      'city_field' => NULL,
      'address_field' => NULL,
      'postal_code_field' => NULL,
      'postal_code_field' => NULL,
      'gender_field' => NULL,
      'birthdate_field' => NULL,
      'debug' => FALSE,
      'custom_omnisend_field' => "field[omnisend_field_id,0]: '[webform_field_machine_name]'",
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $elements = $this->webform->getElementsInitializedAndFlattened();

    // Add the select none option to the list.
    $options = ['' => $this->t('None')];

    // Add every field to the list as a select option.
    foreach ($elements as $key => $element) {
      // Check if the field is an element field and not a layout field.
      if (isset($element['#title'])) {
        $options[$key] = $element['#title'];
      }
    }

    $form['email_field'] = [
      '#title' => $this->t('Email field'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the email field to sync.'),
      '#default_value' => $this->configuration['email_field'],
      '#required' => TRUE,
    ];

    $form['first_name_field'] = [
      '#title' => $this->t('First name field'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the first name field to sync.'),
      '#default_value' => $this->configuration['first_name_field'],
    ];

    $form['last_name_field'] = [
      '#title' => $this->t('Last name field'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the last name field to sync.'),
      '#default_value' => $this->configuration['last_name_field'],
    ];

    $form['country_field'] = [
      '#title' => $this->t('Country'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the country field to sync.'),
      '#default_value' => $this->configuration['country_field'],
    ];

    $form['country_code_field'] = [
      '#title' => $this->t('Country code'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the Country code field to sync. ISO Country code. You can find all country codes in Guides part.'),
      '#default_value' => $this->configuration['country_code_field'],
    ];

    $form['state_field'] = [
      '#title' => $this->t('State'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the State field to sync.'),
      '#default_value' => $this->configuration['state_field'],
    ];

    $form['city_field'] = [
      '#title' => $this->t('City'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the City field to sync.'),
      '#default_value' => $this->configuration['city_field'],
    ];

    $form['address_field'] = [
      '#title' => $this->t('Address'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the Address field to sync.'),
      '#default_value' => $this->configuration['address_field'],
    ];

    $form['postal_code_field'] = [
      '#title' => $this->t('Postal code'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t("Choose the Postal code field to sync. Contact's postal or zip code."),
      '#default_value' => $this->configuration['postal_code_field'],
    ];

    $form['gender_field'] = [
      '#title' => $this->t('Gender'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the Gender field to sync. m - male, f- female'),
      '#default_value' => $this->configuration['gender_field'],
    ];

    $form['birthdate_field'] = [
      '#title' => $this->t('Birth date'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the Birth date field to sync. Format: YYYY-MM-DD. Example: 1981-11-05'),
      '#default_value' => $this->configuration['birthdate_field'],
    ];

    $form['send_welcome_email'] = [
      '#title' => $this->t('Send welcome email'),
      "#type" => "checkbox",
      '#description' => $this->t('Send welcome email'),
      '#default_value' => $this->configuration['send_welcome_email'],
    ];

    // Enable the debug option.
    $form['development'] = [
      '#type' => 'details',
      '#title' => $this->t('Development settings'),
    ];

    $form['development']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging'),
      '#description' => $this->t('If checked, posted submissions will be displayed onscreen to all users.'),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['debug'],
    ];

    $form['development']['custom_omnisend_field'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#title' => $this->t('Omnisend submission data mapping'),
      '#description' => $this->t("Edit the form data that will be sent to the Omnisend site when a webform submission is made.
        The following syntax is required:
        <code>field[omnisend_field_id,0]: '[webform_field_machine_name]'<code>.
        Examples <code>field[345,0]: '[field_machine_name]'<code> or <code>field[%PERS_1%,0]: '[field_machine_name]'<code>.
        More info can be found at the omnisend marketing api 1, contact sync documentation."),
      '#default_value' => $this->configuration['custom_omnisend_field'],
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

    parent::submitConfigurationForm($form, $form_state);

    // Save the configuration settings.
    $this->applyFormStateToConfiguration($form_state);

    // Save debug settings.
    $this->configuration['debug'] = (bool) $this->configuration['debug'];

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    $configuration = $this->getConfiguration();
    $settings = $configuration['settings'];

    $data = $webform_submission->getData();

    // Retrieving the configured omnisend marketing fields from the data.
    $email_key = $settings['email_field'];
    $first_name_key = $settings['first_name_field'];
    $last_name_key = $settings['last_name_field'];
    $country_key = $settings['country_field'];
    $country_code_key = $settings['country_code_field'];
    $state_key = $settings['state_field'];
    $city_key = $settings['city_field'];
    $address_key = $settings['address_field'];
    $postal_code_key = $settings['postal_code_field'];
    $gender_key = $settings['gender_field'];
    $birthdate_key = $settings['birthdate_field'];

    $custom_omnisend_field = $settings['custom_omnisend_field'];

    $send_welcome_email = $this->configuration['send_welcome_email'] ?? FALSE;

    $contact_properties['sendWelcomeEmail'] = $send_welcome_email;

    // Email field doesn't need to be checked, because it's required.
    $contact_properties['email'] = $data[$email_key];

    // Check if first name field has a value.
    if (isset($data[$first_name_key]) && !empty($data[$first_name_key])) {
      $contact_properties['firstName'] = $data[$first_name_key];
    }

    // Check if last name field has a value.
    if (isset($data[$last_name_key]) && !empty($data[$last_name_key])) {
      $contact_properties['lastName'] = $data[$last_name_key];
    }
    // Check if last name field has a value.
    if (isset($data[$country_key]) && !empty($data[$country_key])) {
      $contact_properties['country'] = $data[$country_key];
    }

    if (isset($data[$country_code_key]) && !empty($data[$country_code_key])) {
      $contact_properties['countryCode'] = $data[$country_code_key];
    }

    if (isset($data[$state_key]) && !empty($data[$state_key])) {
      $contact_properties['state'] = $data[$state_key];
    }

    if (isset($data[$city_key]) && !empty($data[$city_key])) {
      $contact_properties['city'] = $data[$city_key];
    }

    if (isset($data[$address_key]) && !empty($data[$address_key])) {
      $contact_properties['address'] = $data[$address_key];
    }

    if (isset($data[$postal_code_key]) && !empty($data[$postal_code_key])) {
      $contact_properties['postalCode'] = $data[$postal_code_key];
    }

    if (isset($data[$gender_key]) && !empty($data[$gender_key])) {
      $contact_properties['gender'] = $data[$gender_key];
    }

    if (isset($data[$birthdate_key]) && !empty($data[$birthdate_key])) {
      $contact_properties['birthdate'] = $data[$birthdate_key];
    }

    $custom_omnisend_fields = Yaml::parse($custom_omnisend_field);

    if (!empty($custom_omnisend_fields)) {
      // Check if the default value was changed.
      if (!in_array('field[omnisend_field_id,0]', $custom_omnisend_fields)) {

        foreach ($custom_omnisend_fields as $key => $value) {
          // Remove the unneeded characters from the webform id.
          $webform_id = str_replace('[', '', $value);
          $webform_id = str_replace(']', '', $webform_id);

          // Check if the mapped field has data to send.
          if (!empty($data[$webform_id])) {
            $contact_properties[$key] = $data[$webform_id];
          }
        }
      }
    }

    if ($settings['debug'] === TRUE) {
      $build = [
        'form_values_label' => ['#markup' => $this->t('Submitted form values are:')],
        'form_values_data' => [
          '#markup' => WebformYaml::encode($data),
          '#prefix' => '<pre>',
          '#suffix' => '</pre>',
        ],
        'campaign_values_label' => ['#markup' => $this->t('Submitted omnisend marketing values are:')],
        'campaign_values_data' => [
          '#markup' => WebformYaml::encode($contact_properties),
          '#prefix' => '<pre>',
          '#suffix' => '</pre>',
        ],
      ];

      $message = $this->renderer->renderPlain($build);
      $this->getLogger('omnisend')->debug(
        'Debugging enabled: </br>' . $message
      );
    }

  }

}
