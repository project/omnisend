<?php

/**
 * @file
 * Functionality for pmnisend module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function omnisend_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the omnisend module.
    case 'help.page.omnisend':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides omnisend API integration.') . '</p>';
      $output .= '<p>' . t('<a href="https://app.omnisend.com/o/my-account/integrations/api-keys" target="_blank">https://app.omnisend.com/o/my-account/integrations/api-keys</a>') . '</p>';

      $output .= '<p>' . t('<a href="https://api-docs.omnisend.com/reference/get_campaigns-campaignid-contacts" target="_blank">https://api-docs.omnisend.com/reference/get_campaigns-campaignid-contacts</a>') . '</p>';

      return $output;

    default:
  }
}

/**
 * Log messages on the system with different log levels.
 *
 * This function logs messages with varying levels of severity,
 * such as alert, warning,
 * debug, and default info, to the Omnisend logger.
 *
 * @param string $log_type
 *   The type of the log message. Possible values are:
 *   - 'alert': for critical alert messages.
 *   - 'warning': for warning messages.
 *   - 'debug': for debugging information.
 *   - 'default': for general informational messages.
 * @param string $message
 *   The message to be logged.
 *
 * @return void
 *   This function does not return any value.
 */
function omnisend_log_messages($log_type, $message) {
  switch ($log_type) {
    case 'alert':
      \Drupal::logger('omnisend')->alert($message);
      break;

    case 'warning':
      \Drupal::logger('omnisend')->warning($message);
      break;

    case 'debug':
      \Drupal::logger('omnisend')->debug($message);
      break;

    default:
      \Drupal::logger('omnisend')->info($message);
      break;
  }
}
